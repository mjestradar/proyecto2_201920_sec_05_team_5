package model.data_structures;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import model.logic.TravelTime;
import model.logic.Zona;
import model.logic.properties;


public class Sort {

	private static Comparator<TravelTime> comparadorActual;
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 * @param hi 
	 * @param mid 
	 * @param lo 
	 * @param aux 
	 */
	public static void ordenarMergeSort( ArrayList<TravelTime>datos, ArrayList<TravelTime> aux, int lo, int mid, int hi ) 
	{
		for (int k = lo; k <= hi; k++)
		
		{
			aux.set(k, aux.get(k));
		}
		
		int i = lo;
		int j = mid+1;

		for (int k = lo; k <= hi; k++)
		{
			if      (i > mid)
				datos.set(k, aux.get(j++) );
			else if (j > hi)             
				datos.set(k,aux.get(i++));
			else if (less(aux.get(j), aux.get(i))) 
				datos.set(k, aux.get(j++) );
			else                      
				datos.set(k,aux.get(i++));
		}
	}
	private static void sortM(ArrayList<TravelTime> datos, ArrayList<TravelTime>aux, int lo, int hi)
	{
		if (hi<=lo)
			return;
		int mid=lo+(hi-lo)/2;
		sortM(datos,aux,lo,mid);
		sortM(datos,aux,mid+1,hi);
		ordenarMergeSort(datos,aux,lo,mid,hi);
	}

	public static void sortM(ArrayList<TravelTime>datos, Comparator<TravelTime>entrada)
	{
		comparadorActual = entrada;
		ArrayList<TravelTime>aux=new ArrayList<TravelTime> ();
		sortM(datos,aux,0,datos.size()-1);
	}



	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 *param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(TravelTime v, TravelTime w)
	{
		return comparadorActual.compare(v, w)<0;
	}

	public static void invertirMuestra( TravelTime[ ] datos )
	{
		TravelTime[] muestraInvertida = new TravelTime[ datos.length ];
		int j = datos.length-1;
		for ( int i = 0 ; i < datos.length-1 ; i++ )
		{
			muestraInvertida[i] = datos[j];
			j--;	
		}	
		datos = muestraInvertida;
	}


}