package model.logic;

public class Nodo implements Comparable<Nodo> {
 
	private int id;
	private double yCord;
	private double xCord;
	
	
	public Nodo (int pId, double pYcord, double pXcord )
	{
		id= pId;
		yCord=pYcord;
		xCord=pXcord;
	
	}
	
	public int darId()
	{
		return id;
	}
    public double darYcord()
    {
    	return yCord;
    }
    public double darXcord()
    {
    	return xCord;
    }

	public int compareTo(Nodo o) {
		// TODO Auto-generated method stub
		if(xCord-o.darXcord()!=0) {
			return xCord-o.darXcord()<0?-1:1;
		}
		else 
			return 
				yCord-o.darYcord()<0?-1:yCord-o.darYcord()==0?0:1;
	}
}
