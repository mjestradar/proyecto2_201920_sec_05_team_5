package model.logic;


import java.awt.AlphaComposite;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.google.gson.*;
import model.data_structures.*;




/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */



	private ArrayList<TravelTime> viajes;
	private ArrayList<Zona> zonas;
	private ArrayList<Nodo> nodos;
    private Queue<Nodo> nodos2;
    private MaxHeapCP<Nodo> cord;


	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		zonas = new ArrayList<>();
		nodos= new ArrayList<>();
		viajes= new ArrayList();
		nodos2= new Queue<Nodo>();
		

	}




	public int[] cargar() {
		int rta =0;
		int rta2=0;
		int rta3=0;

		CSVReader reader = null;
		try 
		{ 
			Gson gson=new Gson();
			BufferedReader br = new BufferedReader(new FileReader("./data/bogota_cadastral.json")); 
			geometry[] geo= gson.fromJson(br, geometry[].class)	;	
			properties[] prop=  gson.fromJson(br,properties[].class); 
			rta=geo.length;	
			for (int j =0; j<rta;j++)
			{
				

			}

			for (int i =1; i<3;i++)
			{	
				reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+i+"-All-MonthlyAggregate.csv"));
				String[] nextLine=reader.readNext();


				while ((nextLine = reader.readNext()) != null) 
				{
					TravelTime elem = new TravelTime(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
							Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
							Double.parseDouble(nextLine[4]));
					
					viajes.add(elem);
					rta2++;

				}
			}

			File f = new File("./data/Nodes_of_red_vial-wgs84_shp.txt");
			FileReader fr= new FileReader(f);
			BufferedReader bf= new BufferedReader(fr);
			while (bf.readLine()!=null)
			{  String word= bf.readLine();
			String[] datos = word.split(",");
			nodos.add(new Nodo(Integer.parseInt(datos[0]),Double.parseDouble(datos[2]), Double.parseDouble(datos[1])));

			}
			rta3=nodos.size();
		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		int[] k ={rta, rta2,rta3};

		return k;
	}


	public char[] darNletrasFreq (int N )
	{   return null;
	}

	public Nodo[] darLimitacionGeografica( double x, double y)
	{
		return null;
	}

	public TravelTime[] tiempoPromRango (int rango1,int rango2)
	{
		return null;
	}

	public IQueue BuscarNzonasNorte (int N)
	{
		
		MaxHeapCP<Zona> respuesta = new MaxHeapCP<Zona>(1000);
		
		for(int i = 1; i < 1000; i++)
		{
				respuesta.agregar(zonas.get(i));
		}
	
		Queue<Zona> retorno = new Queue<Zona>();
		for(int i = 0; i < N; i++)
		{
			retorno.enqueue(respuesta.delMax());
		}
		return retorno;	
	}
	public ArrayList<Nodo> BuscarNodosMallavila (double x, double y)
	{   ArrayList <Nodo> n= new ArrayList<Nodo>();
		Nodo actual = nodos.get(0);
		Queue<Nodo> zonas1 = new Queue<Nodo>();
		zonas1.enqueue(actual);
		RedBlackTree<Nodo, Queue<Nodo>> arbol  = new RedBlackTree<Nodo, Queue<Nodo>>();
		cord = new MaxHeapCP<Nodo>(2500);
		for(int i = 1; i<nodos.size(); i++)
		{
			
				Nodo dato = new Nodo(actual.darId(), actual.darXcord(), actual.darYcord() );
				arbol.put(dato, zonas1);
				cord.agregar(dato);
				actual = nodos.get(i);
				zonas1 = new Queue<Nodo>();
			
		}		
		Nodo dato = new Nodo(actual.darId(), actual.darXcord(), actual.darYcord());
		arbol.put(dato, zonas1);
		cord.agregar(dato);
		for(Nodo e : arbol.keys())
		{
			if(e.darXcord()== x && e.darYcord() == y)
			{
				n.add(e);
			}
		}
		return  n;

	}

	public ArrayList<TravelTime> tiempoEspera (int rango1,int rango2, int N)
	{   ArrayList<TravelTime> tr= new ArrayList<TravelTime>();
		Sort.sortM(viajes, new TravelTime.comparatorInicial());
		TravelTime muestra=viajes.get(0);
		Queue<TravelTime> violaciones = new Queue<TravelTime>();
		SeparateChainingHash<TravelTime,Queue<TravelTime>> tablaHash=new SeparateChainingHash<TravelTime,Queue<TravelTime>>(25);
		for(int i=0;i<viajes.size();i++)
		{
		
			{
				tablaHash.put(new TravelTime(muestra.getDow(), muestra.getDstId(), muestra.getSourceId(), muestra.getMeanTravelTime(), muestra.getStandardDeviationTravelTime()) , violaciones);
				muestra=viajes.get(i);
				violaciones.enqueue(viajes.get(i));
			}
		}
		tablaHash.put(new TravelTime(muestra.getDow(), muestra.getDstId(), muestra.getSourceId(), muestra.getMeanTravelTime(), muestra.getStandardDeviationTravelTime()) , violaciones);
		Iterator<TravelTime> iter = (Iterator<TravelTime>) tablaHash.keys(); 
		int i=0;
		while ( i<N && iter.hasNext() )
		{
			if (rango2>iter.next().getStandardDeviationTravelTime() && iter.next().getStandardDeviationTravelTime()>rango1)
			{
				tr.add(iter.next());
			}
		}	 
		
		return null;	
	}

	public ArrayList<TravelTime> viajesZonaHora(int pzona, int hora )
	{
		ArrayList<TravelTime> tr= new ArrayList<TravelTime>();
		Sort.sortM(viajes, new TravelTime.comparatorInicial());
		TravelTime muestra=viajes.get(0);
		Queue<TravelTime> violaciones = new Queue<TravelTime>();
		SeparateChainingHash<TravelTime,Queue<TravelTime>> tablaHash=new SeparateChainingHash<TravelTime,Queue<TravelTime>>(25);
		for(int i=0;i<viajes.size();i++)
		{
		
			{
				tablaHash.put(new TravelTime(muestra.getDow(), muestra.getDstId(), muestra.getSourceId(), muestra.getMeanTravelTime(), muestra.getStandardDeviationTravelTime()) , violaciones);
				muestra=viajes.get(i);
				violaciones.enqueue(viajes.get(i));
			}
		}
		tablaHash.put(new TravelTime(muestra.getDow(), muestra.getDstId(), muestra.getSourceId(), muestra.getMeanTravelTime(), muestra.getStandardDeviationTravelTime()) , violaciones);
		Iterator<TravelTime> iter = (Iterator<TravelTime>) tablaHash.keys(); 
		int i=0;
		while ( iter.hasNext() )
		{
			if (iter.next().getSourceId()== pzona && iter.next().getDow()== hora)
			{
				tr.add(iter.next());
			}
		}	 
		
		return tr;
	}
	public void viajesDadorangoZona (Zona pZona, int rango1, int rango2)
	{
      
	}

	public IQueue nZonas (int N)
	{
       MaxHeapCP<Zona> respuesta = new MaxHeapCP<Zona>(1000);
		
		for(int i = 1; i < 1000; i++)
		{
				respuesta.agregar(zonas.get(i));
		}
	
		Queue<Zona> retorno = new Queue<Zona>();
		for(int i = 0; i < N; i++)
		{
			retorno.enqueue(respuesta.delMax());
		}
		return retorno;
	}
}