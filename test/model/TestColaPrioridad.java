package model;

import junit.framework.TestCase;
import model.data_structures.MaxHeapCP;
import model.logic.*;

public class TestColaPrioridad extends TestCase
{


	MaxHeapCP <Integer> heapCP;

	MaxHeapCP <TravelTime> heapLocation;
	public void setUp()
	{

		heapCP= new MaxHeapCP <Integer>(6);
		heapCP.agregar(4);
		heapCP.agregar(2);
		heapCP.agregar(1);
	}
	public void setUp2()
	{

	    heapLocation = new MaxHeapCP<TravelTime>(10);
		for(int i = 0; i < 10; i++)
		{
		
			heapLocation.agregar(new TravelTime(i,i,i,i,i));
		}
		
	}




	public void testAgregarHeap()
	{
		heapCP.agregar(0);
		assertEquals("No se agreg� el elemento", 0, heapCP.darElemento(4).intValue());
	}
    public void testAgregarHeap2()
    {
    	setUp2();
    	TravelTime dato = new TravelTime(11, 2, 11,1,1);
		heapLocation.agregar(dato);
		assertEquals("Error al cargar un location en Heap", dato.getMeanTravelTime(), heapLocation.darElemento(1).getMeanTravelTime());
    }
	public void testDelMaxHeap()
	{
		heapCP.agregar(6);
		assertEquals("No se elimin� el elemento con mayor prioridad", 6, heapCP.delMax().intValue());
	}
	public void testDelMaxHeap2()
	{
		setUp2();
		TravelTime dato = new TravelTime(11, 2, 11,1,1);
		heapLocation.agregar(dato);
		assertEquals("Error al cargar un location en Heap", dato.getMeanTravelTime(), heapLocation.delMax().getMeanTravelTime());
	}

	public void testSizeQHeap() throws Exception
	{
		assertEquals("El tama�o no es el esperado",3,heapCP.darNumElementos());
	}
}