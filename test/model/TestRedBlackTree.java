package model;

import junit.framework.TestCase;
import model.data_structures.*;

public class TestRedBlackTree extends TestCase
{
	private RedBlackTree<String,Integer> prueba;

	public void setUp()
	{
		prueba=new RedBlackTree<String,Integer>();

		for(int i=2;i<10001;i++)
			prueba.put("b"+i, i-1);
		prueba.put("a", 0);
		 

	}

	public void testEmpty()
	{
		assertEquals(false,prueba.isEmpty());
	}
	

	
	public void testContains()
	{
		assertEquals(false,prueba.contains("C999"));
		assertEquals(true,prueba.contains("b999"));
	}
	
	public void testGet()
	{
		assertEquals((int)999, (int)prueba.get("b1000"));
	}

	public void testPut()
	{
		prueba.put("c201", 42);
		assertEquals((int)42,(int)prueba.get("c201"));
	}
	

	
	public void testMax()
	{
		assertEquals("b9999",prueba.max());
	}

	public void testMin()
	{
		assertEquals("a",prueba.min());
	}
	
	

	
	
}
