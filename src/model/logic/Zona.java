package model.logic;

import java.util.Comparator;



public class Zona implements Comparable<Zona>{

	
	private geometry geo;
	private properties prop;
	
	public Zona(geometry pGeo, properties pProp)
	{
		geo=pGeo;
		prop=pProp;
	}
	
	public geometry darGeo()
	{
		return geo;
	}
	
	public properties darProp()
	{
		return prop;
	}

	@Override
	public int compareTo(Zona o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static class comparatorInicial implements Comparator <Zona>
	{
		public int compare(Zona arg0, Zona arg1 )
		{
			return arg0.darProp().compareTo(arg1.prop);
		}
	
	}

	public static class comparatornorte implements Comparator <Zona>
	{
		public int compare(Zona arg0, Zona arg1 )
		{
			return arg0.darGeo().compareTo(arg1.darGeo());
		}
	}
}
