package model.logic;

import model.data_structures.Sort;


public class geometry implements Comparable <geometry>
{
  private String type;	
  
  private double [][] coordinates;

  public geometry(double [][] cor, String pal )
  {
	  type= pal;
	  coordinates=cor;
  }
  
  public String darType()
  {
	  return type;
  }
  
  public double[][] darCordenadas()
  {
	  return coordinates;
  }

@Override
public int compareTo(geometry o) {
	double max1 = 0;
	for ( int i=0; i< darCordenadas()[2].length; i++ )
	{
		if (darCordenadas()[2][i]>max1)
		{
			max1=darCordenadas()[2][i];
		}
	}
	
	double max2 = 0;
	for ( int i=0; i< o.darCordenadas()[2].length; i++ )
	{
		if (o.darCordenadas()[2][i]>max1)
		{
			max2=darCordenadas()[2][i];
		}
	}
	
	return (int)(max1-max2);
	// TODO Auto-generated method stub
	
}

 public int darNorte()
 {
	 double max1 = 0;
		for ( int i=0; i< darCordenadas()[2].length; i++ )
		{
			if (darCordenadas()[2][i]>max1)
			{
				max1=darCordenadas()[2][i];
			}
		}
		return (int)max1;
 }
  
}


